# - Try to find Gammu library
# Once done this will define
#
#  GAMMU_FOUND - System has Gammu
#  GAMMU_INCLUDE_DIR - The Gammu include directory
#  GAMMU_LIBRARIES - The libraries needed to use Gammu
#  GAMMU_DEFINITIONS - Compiler switches required for using Gammu

# Copyright (c) 2009, Fathi Boudra <fabo@debian.org>
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.

if(GAMMU_INCLUDE_DIR AND GAMMU_LIBRARIES)
    # in cache already
    set(Gammu_FIND_QUIETLY TRUE)
endif(GAMMU_INCLUDE_DIR AND GAMMU_LIBRARIES)

if(NOT WIN32)
    # use pkg-config to get the directories and then use these values
    # in the FIND_PATH() and FIND_LIBRARY() calls
    find_package(PkgConfig)
    pkg_check_modules(PC_GAMMU gammu)
    set(GAMMU_DEFINITIONS ${PC_GAMMU_CFLAGS_OTHER})
endif(NOT WIN32)

find_path(GAMMU_INCLUDE_DIR
          NAMES gammu.h
          HINTS ${PC_GAMMU_INCLUDEDIR} ${PC_GAMMU_INCLUDE_DIRS}
          PATH_SUFFIXES gammu)

find_library(GAMMU_LIBRARIES
             NAMES Gammu
             HINTS ${PC_GAMMU_LIBDIR} ${PC_GAMMU_LIBRARY_DIRS})

# handle the QUIETLY and REQUIRED arguments and set GAMMU_FOUND to TRUE if 
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Gammu DEFAULT_MSG GAMMU_LIBRARIES GAMMU_INCLUDE_DIR)

mark_as_advanced(GAMMU_INCLUDE_DIR GAMMU_LIBRARIES)
